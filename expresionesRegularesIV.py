# -*- coding: utf-8 -*-
"""
Created on Sat Nov  9 20:48:36 2019

@author: Carlos García
"""

import re

nom1 ="Karla Sanchez"

nom2 ="Martin E"

nom3 ="Sonia Blade"

nom4 ="karla Sausage"

if re.match("Karla",nom1):
    print("Hola {}".format(nom1))
else:
    print("Error")

if re.match("Karla",nom4, re.IGNORECASE): #Esro ignora el ccase sensitive
    print("Hola {}".format(nom4))
else:
    print("Error")
    
if re.match(".arla",nom1): #el punto hace que busque una cadena de texto
    print("Hola coicidencia {}".format(nom1))
else:
    print("Error")
    
if re.search("E",nom2):
    print("Hola {}".format(nom2))
else:
    print("Error")