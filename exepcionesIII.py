#Crear una exepcion predeterminada
def evaluar_edad(edad):

    if edad < 0:
        raise TypeError("No se permiten edades negativas")

    if edad < 20:
         return "Eres muy joven"
    elif edad < 40:
        return "Eres aun joven"
    elif edad  < 65:
        return "Ya estas maduro"
    elif edad < 100:
         return "Cuidate" 

print(evaluar_edad(18))

#Le damos nombre a los errores

import math
def calculoraiz(num1):
    if num1<0:
        raise ValueError ("El numero no puede ser negativo")
    else:
        return math.sqrt(num1)

op1=(int(input("Introduce un numero:...")))

try:
    print(calculoraiz(op1))
except ValueError as Error_Numero_Negativo:
    print(Error_Numero_Negativo)

print("Programa terminado")
