# -*- coding: utf-8 -*-
"""
Created on Sat Nov  9 21:24:29 2019

@author: Carlos García
"""

#Decoradores con parametros

def funcion_decoradora(funcion_parametro): #los parametros son otras funciones exteriores
    
    def funcion_interior(*args,**kwargs): #args recive varios parametros
        
        #Acciones adicionales que decoran
        
        print("Vamo a realizar un calculo")
        
        funcion_parametro(*args,**kwargs)  #kargs es para parametros con claves
        
        #Acciones que decoran
        
        print("Hemos terminado el calculo")
        print("-" * 20)
        
    return funcion_interior

@funcion_decoradora
def suma(num1,num2):
    print("Operacion Suma")
    print(num1+num2)
    
@funcion_decoradora
def resta(num1,num2):
    print("Operacion Resta")
    print(num1-num2)
    
@funcion_decoradora
def potencia(base,exponente):
    print("Operacion Potencia")
    print(pow(base,exponente))
    

suma(7,10)

resta(20,10)

potencia(base=5,exponente=2) #Argumentos con claves, por eso utilizamos **kargs