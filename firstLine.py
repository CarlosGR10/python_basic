a = 'Hola mundo'
print(a)

#string
print('Hola')

#Integer
print(10)

#Float
print(13.1)

#Listas
print([1,2,3,4,5])
print(["Hola","Adios"])

#Tuples
print((1,2,3,4,5))

#Diccionarios
print({
    "name":"Rayan",
    "lastname":"Ray",
    "code":20
})

#directorio de metodos
print(dir(a))

print(a[3])
print(a[-2])

n1 = 'feel'

#print(f"i Am {n1}") #para versiones de python 3
