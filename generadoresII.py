def devuelve_ciudades(*ciudades): #el asterisco en los parametros es que va recibir un numero indeterminado de argumentos en forma de tupla
    for elemento in ciudades:
        #for subElemento in elemento: --> se sustituye por el yield from
            yield from elemento
ciudades_devueltas=devuelve_ciudades("Madrid","Barcelona","Valencia")

print(next(ciudades_devueltas))
print(next(ciudades_devueltas))