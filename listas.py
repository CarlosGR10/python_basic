colors = ['red','green','blue','orange']

colors2 = list(('grey','black','pink'))

r = list(range(1,101))
print(r)

print(colors)
print(colors2)

print('blue' in colors)
print(colors[3])
print(len(colors))

colors[2] = 'yelow'
print(colors)

colors.append('violet')
print(colors)

colors.extend(list(colors2))
print(colors)

colors.insert(2,'gold')
print(colors)

colors.pop(-1)
print(colors)

colors.remove('violet')
print(colors)

colors.sort()
print(colors)

print(colors.index('gold'))

print(colors.count('green'))