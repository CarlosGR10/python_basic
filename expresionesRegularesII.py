# -*- coding: utf-8 -*-
"""
Created on Sat Nov  9 18:55:48 2019

@author: Carlos García
"""

import re

#Anclas y clases de caracteres

lista_nombres=['Ana Gómez',
               'Maira Ramirez',
               'Sandra López',
               'Santiago Ramirez',
               'Sandra Fernández',
               'Maxwell',
               'niños',
               'niñas',
               'camión',
               'camion']

for elemento in lista_nombres:
    if re.findall('^Sandra',elemento): #El caracter ^ busca las primeras coicidencias
        print(elemento)

for eles in lista_nombres:
    if re.findall('Ramirez$',eles): #El caracter $ busca las coicidencias finales
        print(eles)
        
for elem in lista_nombres:
    if re.findall('[x]',elem): #Los corchetes buscan coicidencias en toda la lista
        print(elem)
        
for elem in lista_nombres:
    if re.findall('niñ[oa]s',elem): # Busqueda combinada
        print(elem)

for elem in lista_nombres:
    if re.findall('cami[oó]n',elem): # Busqueda de caracteres latinos
        print(elem)   