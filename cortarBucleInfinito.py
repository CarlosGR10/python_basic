import math

print("Programa Raiz Cuadrada")
numero = int(input("Introduce un numero...."))

intentos=0

while numero<0:
    print("No hay raices negativas")

    if intentos == 3:
        print("Fin, terminarón los intentos")
        break;

    numero = int(input("Introduce un numero...."))
    if numero<0:
        intentos=intentos+1

if intentos < 3:
    resultado=math.sqrt(numero)
    print("La raiz es..."+str(numero)+" es "+str(resultado))