#Modo Normal

def generaPares(limites):
    num=1

    miLista=[]

    while num<limites:

        miLista.append(num*2)

        num=num+1
    
    return miLista

print(generaPares(20))

#Objeto interable (Generador)

def genera_pares(limite):
    num=1

    while num<limite:

        yield num*2

        num=num+1

devuelve_pares=genera_pares(10) #entra eb un estado de suspenciós
print("Estos son valores optimizados")
print(next(devuelve_pares))
print(next(devuelve_pares))
print(next(devuelve_pares))

#for i in devuelve_pares:
 #   print(i)
