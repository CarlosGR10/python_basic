clients = 'pablo,ricardo,'

def create_client(client_name):
    global clients # Declaramos una variable global

    clients += client_name
    _add_comma()

def list_clients():
    global clients #Creamos una variable global
    print(clients)



def _add_comma():
    global clients
    clients += ','

if __name__ == '__main__':
    list_clients() #lista sin agregar
    clients += 'Carlos'
    list_clients() #Lista de agregado completo