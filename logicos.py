#condicionales Logicos AND & OR
print("Condicionales de logica")

distancia=int(input("km..."))
print("distancia")

hermanos=int(input("Hermanos..."))
print("hermanos")

salario=int(input("Salario..."))
print("salario")

if distancia>20 and hermanos>3 or salario<200:
    print("Tienes derecho a una beca")
else:
    print("No cumples con los requisitos")

#Condicional In

print("Asignaturas de una carrera")
print("Informatica-Soporte-seguridad")

opcion=input("Escoje una asignatura:...")

asignatura=opcion #Este metodo es util para convertir el texto a mayusculas

if asignatura in ("informatica","soporte","seguridad"):
    print("Asignatura elegida es..." + asignatura.upper())
else:
    print("Esa asignatura no existe")