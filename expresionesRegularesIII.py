# -*- coding: utf-8 -*-
"""
Created on Sat Nov  9 19:42:03 2019

@author: Carlos García
"""

#Busqueda por rangos

import re

lista_nombres=["Newton",
               "Einstein",
               "Steve",
               "Mark",
               "Gandi",
               "Rob",
               "Ostin",
               "Darwin"]

for elemento in lista_nombres:
    if re.findall('[a-d]',elemento): # Rango del abecedario
        print("Contienen de la a hasta la d : {}".format(elemento))
        
print("--------------------------------")       

for elemento in lista_nombres:
    if re.findall('^[O-Z]',elemento): # Rango del abecedario al inicio
        print("inician con rango O hasta Z : {}".format(elemento))
        
print("--------------------------------")     
    
for elemento in lista_nombres:
    if re.findall('[e-i]$',elemento): # Rango del abecedario al final
        print("finalizan del rango e hasta i : {}".format(elemento))
        
        
codigo = ['Ma1',
          'Se2',
          'Ma2',
          'Ba1',
          'Ma:8',
          'Ma3',
          'Va1',
          'Va2',
          'Va3',
          'Ma.5',
          'Ma4',
          'Ma5',
          'MaA',
          'Se.6',
          'Ma6',
          'MaB',
          'MaC',
          'MaD']  

print("--------------------------------") 
for elem in codigo:
    if re.findall("Ma[0-3]",elem): #Buscamos los Ma de 0 a 3
        print(elem)

print("--------------------------------")     
for elem in codigo:
    if re.findall("Ma[^0-3]",elem): #Buscamos los Ma diferentes de 0 a 3
        print(elem)
        
print("--------------------------------")     
for elem in codigo:
    if re.findall("Ma[0-6A-C]",elem): #Buscamos los Ma de 0 a 3 y de A a B
        print(elem)
        
print("--------------------------------")     
for elem in codigo:
    if re.findall("Ma[.:]",elem): #Buscamos los Ma que contengan punto y 2 puntos
        print(elem)
        