import doctest
doctest.testmod()

def compruebaMail(mailUsuario):
    '''La funcion compruebaMail evalua un mail si ha recibido un mail con un @
       >>>compruebaMail('Carlos@python.com')
       True

       >>>compruebaMail('Carlospython.com@')
       False

       >>>compruebaMail('Carlospython.com')
       False

       >>>compruebaMail('Carlospython@.com')
       False'''

    arroba=mailUsuario.count('@')

    if(arroba != 1 or mailUsuario.rfind('@')==(len(mailUsuario)-1) or mailUsuario('@')==0):
        return False
    else:
        return True
print(compruebaMail.__doc__)