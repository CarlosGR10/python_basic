# -*- coding: utf-8 -*-
"""
Created on Sat Nov  9 18:01:14 2019

@author: Carlos García
"""

import re

cadena = "Hola, mundo vamos a aprender expresiones regulares"

textoBuscar=input("Palabra a buscar? ")

if re.search(textoBuscar,cadena) is not None:
    print(cadena)
    print("Sí esta la palabra {} en la cadena".format(textoBuscar)) 
else:
    print(cadena)
    print("La palabra {} no esta".format(textoBuscar))


'''
print(re.search("TextoABuscar",cadena))
'''

#------------------------------------------------------------------------------

frase = "La pizza es lo mejor del mundo"

buscarPalabra="pizza"

buscarPalabra = re.search(buscarPalabra, frase)

print("-" * 10)

print(buscarPalabra.start()) #El caracter de donde comienza

print(buscarPalabra.end()) #El caracter donde finaliza

print(buscarPalabra.span()) #Busca el primer y ultimo caracter en una tupla


#------------------------------------------------------------------------------

textLarge= "Vamos a aprender Python, el lenguaje mas cool que existe.Python es un buen lenguaje para iniciar a programar"

buscar="Python"

print("-" * 10)

print(re.findall(buscar, textLarge)) #findall -> Busca todas las coicidencias de esa palabra y devuelve una lista

print(len(re.findall(buscar, textLarge))) #Con len contamos las coincidencias
