'''from modulos(carpeta) import funciones_matematicas'''

class Areas:
    '''Esta clase calcula diferentes areas'''
    def areaCuadrado(lado):
        '''Esta funcion: Calcula el area de un cuadrado'''
        return "el area de un cuadrado es: {}".format(lado*lado)

    def areaTriangulo(base,altura):
        '''Esta funcion: Calcula el area de un triangulo'''
        return "el area del tringulo es: {}".format(base*altura/2)

'''print(areaCuadrado(5))
print(areaCuadrado.__doc__)''' #documenta una funcion

'''help(Areas.areaTriangulo)''' #documenta una funcion dentro de una clase()

'''help(funciones_matematicas)''' #documeta modulos llamados

help(Areas)