#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 14 08:35:33 2019

@author: carlos
"""

def foreign_exchange_calculator(ammount):
    mex_to_col_rate = 145.97
    calcular = mex_to_col_rate * ammount
    return calcular

def run():
    print("Calculadora de Divisas")
    print('convierte los pesos mexicanos a pesos colombianos')
    print("<---------->")
    
    ammount = float(input("Ingresa la cantidad de pesos MX:..."))
  
    result = foreign_exchange_calculator(ammount)
    
    print('${} pesos mexicanos son ${} pesos colombianos'.format(ammount, result))
    print("<--Calculador de divisas>")


if __name__ == '__main__':
    run()
    print('final {}')