s = 30

if s < 30:
    print('La letra s es menor que 30')
else:
    print('la letra s es mayor que 30')

color = 'azul'

if color == 'rojo':
    print('El color es rojo')
elif color == 'azul':
    print('Azuul....Como el mar azul')
else:
    print('No se declaro la variable con ese color')

nombre = 'carlos'
apellido = 'Garcia'

if nombre == 'carlos':
    if apellido == 'Garcia':
        print('Tu eres Carlos Garcia')
    else:
        print('Tu no eres Carlos Garcia')
else:
    print('Tu no eres Carlos')
