# -*- coding: utf-8 -*-
"""
Created on Sat Nov  9 21:09:08 2019

@author: Carlos García
"""

#Decoradores: Funcionalidades que añaden funcionalidades a otras funciones

def funcion_decoradora(funcion_parametro): #los parametros son otras funciones exteriores
    
    def funcion_interior():
        
        #----------Acciones adicionales que decoran-----------
        
        print("Vamo a realizar un calculo")
        
        funcion_parametro() 
        
        #----------Acciones que decoran-------------
        
        print("Hemos terminado el calculo")
        print("-" * 20)
        
    return funcion_interior

@funcion_decoradora
def suma():
    print("Operacion Suma")
    print(1+2)
    
@funcion_decoradora
def resta():
    print("Operacion Resta")
    print(10-5)
    

suma()

resta()